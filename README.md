# README #


The purpose of this app was and still is to understand how Tesseract works.
[Tesseract](https://code.google.com/p/tesseract-ocr/) in an OCR(Optical Character Recognition) engine. It has been developped at HP Labs a long ago but is now maintained and improved by Google.

To achieve this, I've been using this project here:
https://github.com/rmtheis/tess-two

### What is this repository for? ###

The repo contains only my app with a simple interface to test Tesseract features. You have to download separately the tess-two project and follow the different steps on the page to build it.

![device-2014-06-03-191547.png](https://bitbucket.org/repo/anbyp7/images/1823280884-device-2014-06-03-191547.png)

### Requirements ###

1. Usual Android Dev Environment (JDK, SDK, Eclipse, IntelliJ...)
2. NDK installed and configured to build the Native part of the libraries

### How do I get set up? ###

1. Clone tess-two repo
2. Follow the steps on the page to build the libs
3. Import tess-two project...and eyes-two project(optional but really cool features) to your workspace

4. Clone DB_Test Repo
5. Import the project into your workspace
6. Tess-two and eyes-two are Library Projects...add them as libraries to the DB_Test project

At launch, the app check and copy if needed, the language(eng) data file to your phone so you don't have to care about that :)

### To know ###

* ONLY the "Use camera" button in front of the Price input is available!
* The recognition process can take a some time depending on the clarity of the text on the image

### How to get a good output ###

Factors at play in the quality of the output:

* Brightness
* Text font
* Text color and background color
* Text Size
* Text orientation
* The dictionnary used(words included, patterns...)

So, don't get mad if it doesn't show you what it's supposed to show at the first try