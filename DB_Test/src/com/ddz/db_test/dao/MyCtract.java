package com.ddz.db_test.dao;

import android.provider.BaseColumns;

public final class MyCtract {

	// *******************************************************************************//
	// IMPORTANT TO READ - IMPORTANT TO READ - IMPORTANT TO READ - IMPORTANT TO READ  //
	// *******************************************************************************//
	// If a table contains a column of type INTEGER PRIMARY KEY, then that column     //
	// becomes an alias for the ROWID. Means that the method SQLiteDatabase.insert()  //
	// which returns the rowId of the added object will return the _ID of the object  //
	////////////////////////////////////////////////////////////////////////////////////
	
	// We begin with information relative to the DB itself
	
	// // Nothing to do with SQLite version, it's for Upgrade purpose (if the DB is modified later)
	public static final int DATABASE_VERSION		= 1; 
	public static final String DATABASE_NAME		= "MyDBTest.db";
	private static final String TEXT_TYPE			= " TEXT";
	private static final String REAL_TYPE			= " REAL"; //Float value
	private static final String INT_TYPE			= " INTEGER";
	private static final String COMMA_SEP			= ", ";
	
	// FOREIGN KEY CONSTRAINTS ARE DISABLED BY DEFAULT so...
	public static final String ENABLE_FOREIGN_KEYS			= "PRAGMA foreign_keys=ON;";
	
	// If return 0, then support is OFF and have to be turned ON
	// If return 1, then support is ON, nothing to do!
	// If return NOTHING, then either SQLite version is older than 3.6.19
	//						OR it was compiled with SQLITE_OMIT_FOREIGN_KEY
	//						OR SQLITE_OMIT_TRIGGER is defined
	public static final String IS_FOREIGN_KEY_SUPPORTED 	= "PRAGMA foreign keys";
	
	public MyCtract(){}
	
	
	// ************** SHOP TABLE***************//
	public static abstract class Shop implements BaseColumns {
		
		public static final String TABLE_NAME 		= "shop";
		public static final String COL1_NAME		= "name";
		public static final String IDX_NAME			= "idxOnName";
		
		public static final String[] COLUMNS		= {_ID, COL1_NAME};
		public static final String[] COLUMNS_NO_ID	= {COL1_NAME};
		public static final String[] COLUMN_ID_ONLY = {_ID};
		
		public static final String CREATE_TABLE 	= 
				"CREATE TABLE " + TABLE_NAME + " (" +
				_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				COL1_NAME + TEXT_TYPE + ")";
		public static final String CREATE_NAME_INDEX = 
				"CREATE UNIQUE INDEX IF NOT EXISTS " + IDX_NAME +
				" ON " + TABLE_NAME + " (" + COL1_NAME + ")";
		public static final String DELETE_INDEX		= 
				"DROP INDEX IF EXISTS " + IDX_NAME;
		public static final String DELETE_TABLE = 
				"DROP TABLE IF EXISTS " +TABLE_NAME;
	}
		
	
	// ************** PRODUCT TABLE***************//
	public static abstract class Product implements BaseColumns {
		
		public static final String TABLE_NAME		= "product";
		public static final String COL1_NAME		= "name";
		public static final String COL2_SHOP_ID		= "shop_id";
		public static final String COL3_BARCODE		= "barcode";
		public static final String COL4_PRICE		= "price";
		
		public static final String[] COLUMNS			= {_ID, COL1_NAME, COL2_SHOP_ID, COL3_BARCODE, COL4_PRICE};
		public static final String[] COLUMNS_NO_ID		= {COL1_NAME, COL2_SHOP_ID, COL3_BARCODE, COL4_PRICE};
		public static final String[] COLUMNS_NO_SHOPID	= {_ID, COL1_NAME, COL3_BARCODE, COL4_PRICE};
		public static final String[] COLUMN_ID_ONLY 	= {_ID};
	
		
		// When a foreign key is defined:
		// - Insert a row in the Dependent/Child Table that doesn't correspond to any row in the Parent Table will fail
		// - Delete a row in the Parent Table when it corresponds rows in the Dependent Table will fail
		// - EXCEPTION: If Foreign Key column is NULL, no corresponding entry in the Parent Table is required
		public static final String CREATE_TABLE		= "CREATE TABLE " +
				TABLE_NAME + " (" +
				_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				COL1_NAME + TEXT_TYPE + COMMA_SEP +
				COL2_SHOP_ID + INT_TYPE + COMMA_SEP +
				COL3_BARCODE + INT_TYPE + COMMA_SEP +
				COL4_PRICE + REAL_TYPE + COMMA_SEP +
				"FOREIGN KEY(" + COL2_SHOP_ID + ") REFERENCES " +
				Shop.TABLE_NAME + "(" + Shop._ID + "))";
		public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " +
				TABLE_NAME;
		
	}
	
	
}
