package com.ddz.db_test.dao;

import java.util.ArrayList;

import com.ddz.db_test.model.Product;
import com.ddz.db_test.model.Shop;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyHelper extends SQLiteOpenHelper {

	private static MyHelper mInstance;
	
//	public MyHelper(Context context, String name, CursorFactory factory,
//			int version) {
//		super(context, name, factory, version);
//	}
	private MyHelper(Context context){
		super(context, MyCtract.DATABASE_NAME, null, MyCtract.DATABASE_VERSION);
	}

	
	public static MyHelper getInstance(Context ctx) {
		
		if(mInstance == null){
			mInstance = new MyHelper(ctx.getApplicationContext());
		}
		return mInstance;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(MyCtract.Shop.CREATE_TABLE);
		db.execSQL(MyCtract.Shop.CREATE_NAME_INDEX);
		db.execSQL(MyCtract.Product.CREATE_TABLE);

	}
	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if(!db.isReadOnly()){
			db.execSQL(MyCtract.ENABLE_FOREIGN_KEYS);
		}
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(MyCtract.Shop.DELETE_INDEX);
		db.execSQL(MyCtract.Product.DELETE_TABLE);
		db.execSQL(MyCtract.Shop.DELETE_TABLE);
		onCreate(db);

	}
	
	
//===================================================================
//**************************CRUD OPERATIONS**************************
//===================================================================


	public static abstract class ShopDAO{
		
		public static Shop addShop(String shopName){
			
			SQLiteDatabase db = mInstance.getWritableDatabase();
			
			Shop newShop = new Shop(shopName);
			
			ContentValues values = new ContentValues();
			values.put(MyCtract.Shop.COL1_NAME, newShop.getName());
			
			// return the _ID (see MyCtract.java comments)
			long newShopId = db.insert(MyCtract.Shop.TABLE_NAME, null, values);
			Log.d("CURSOR TEST", "New shop row_id(=_ID): "+ newShopId);
			
			newShop.setId(newShopId);

			db.close();
			
			return newShop;
		}
		public static Shop getShopById(long shopId){
			
			SQLiteDatabase db = mInstance.getReadableDatabase();
			
			Cursor mCursor = db.query(MyCtract.Shop.TABLE_NAME, MyCtract.Shop.COLUMNS_NO_ID, "_ID = ?", new String[]{String.valueOf(shopId)}, null, null, null);
			Shop aShop = new Shop(shopId, mCursor.getString(0));
				
			mCursor.close();
			db.close();
			
			return aShop;
		}
		public static ArrayList<Shop> getAllShops(){
			
			SQLiteDatabase db = mInstance.getReadableDatabase();
			ArrayList<Shop> allShops = new ArrayList<Shop>();
			
			Cursor mCursor = db.query(MyCtract.Shop.TABLE_NAME, MyCtract.Shop.COLUMNS, null, null, null, null, null);
			
			while(mCursor.moveToNext()){
				allShops.add(new Shop(mCursor.getLong(0), mCursor.getString(1)));
			}
			
			mCursor.close();
			db.close();
			return allShops;
			
		}
		public static int updateShop(Shop updatedShop){
			
			SQLiteDatabase db = mInstance.getWritableDatabase();
			
			ContentValues values = new ContentValues();
			values.put(MyCtract.Shop.COL1_NAME, updatedShop.getName());
			
			int i = db.update(MyCtract.Shop.TABLE_NAME, values, "_ID = ?", new String[]{String.valueOf(updatedShop.getId())});
			
			db.close();
			return i;
		}
		public static int deleteShop(Shop toDelete){
			
			SQLiteDatabase db = mInstance.getWritableDatabase();
			
			int i = db.delete(MyCtract.Shop.TABLE_NAME, "_ID = ?", new String[]{String.valueOf(toDelete.getId())});
			
			db.close();
			return i;
		}
		
	}
	
	public static abstract class ProdDAO{
		
		public static Product addProd(String prodName, long prodShopId, int prodBCode, Float prodPrice){
			
			SQLiteDatabase db = mInstance.getWritableDatabase();
			Product newProd = new Product(prodName, prodShopId, prodBCode, prodPrice);
			
			ContentValues values = new ContentValues();
			values.put(MyCtract.Product.COL1_NAME, prodName);
			values.put(MyCtract.Product.COL2_SHOP_ID, prodShopId);
			values.put(MyCtract.Product.COL3_BARCODE, prodBCode);
			values.put(MyCtract.Product.COL4_PRICE, prodPrice);
			
			// return the ROWID = _ID (see MyCtract.java comments)
			long newProdId = db.insert(MyCtract.Product.TABLE_NAME, null, values);
			newProd.setId(newProdId);
			
			db.close();
			return newProd;
		}
		public static Product getProdById(long prodId){
			
			SQLiteDatabase db = mInstance.getReadableDatabase();
			
			Cursor csr = db.query(MyCtract.Product.TABLE_NAME, MyCtract.Product.COLUMNS_NO_ID, "_ID = ?", new String[]{String.valueOf(prodId)}, null, null, null);
			Product aProd = new Product(prodId, csr.getString(0), csr.getLong(1), csr.getInt(2), csr.getFloat(3));
			
			csr.close();
			db.close();
			return aProd;
		}
		public static ArrayList<Product> getAllProdsFromShopX(long shopId){
			
			SQLiteDatabase db = mInstance.getReadableDatabase();
			ArrayList<Product> allProdsFromShopX = new ArrayList<Product>();
			
			Cursor csr = db.query(MyCtract.Product.TABLE_NAME, MyCtract.Product.COLUMNS_NO_SHOPID, "SHOP_ID = ?", new String[]{String.valueOf(shopId)}, null, null, null);
			while(csr.moveToNext()){
				allProdsFromShopX.add(new Product(csr.getLong(0), csr.getString(1), shopId, csr.getInt(2), csr.getFloat(3)));
			}
			
			csr.close();
			db.close();
			return allProdsFromShopX;
		}
		public static ArrayList<Product> getAllProds(){
			SQLiteDatabase db = mInstance.getReadableDatabase();
			ArrayList<Product> allProds = new ArrayList<Product>();
			
			Cursor csr = db.query(MyCtract.Product.TABLE_NAME, MyCtract.Product.COLUMNS, null, null, null, null, null);
			while(csr.moveToNext()){
				allProds.add(new Product(csr.getLong(0), csr.getString(1), csr.getLong(2), csr.getInt(3), csr.getFloat(4)));
			}
			
			csr.close();
			db.close();
			return allProds;
		}
		public static int updateProd(Product updatedProd){
			
			SQLiteDatabase db = mInstance.getWritableDatabase();
			ContentValues val = new ContentValues();
			val.put(MyCtract.Product.COL1_NAME, updatedProd.getName());
			val.put(MyCtract.Product.COL2_SHOP_ID, updatedProd.getShopId());
			val.put(MyCtract.Product.COL3_BARCODE, updatedProd.getBarcode());
			val.put(MyCtract.Product.COL4_PRICE, updatedProd.getPrice());
			
			int i = db.update(MyCtract.Product.TABLE_NAME, val, "_ID = ?", new String[]{String.valueOf(updatedProd.getId())});
			
			db.close();
			return i;
		}
		public static int deleteProd(Product toDelete){
			
			SQLiteDatabase db = mInstance.getWritableDatabase();
			int i = db.delete(MyCtract.Product.TABLE_NAME, "_ID = ?", new String[]{String.valueOf(toDelete.getId())});
			
			db.close();
			return i;
		}
		
		
	}
	
}
