package com.ddz.db_test.adapter;

import java.util.ArrayList;

import com.ddz.db_test.R;
import com.ddz.db_test.model.Shop;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ShopSpinnerAdapter extends BaseAdapter {

	private ArrayList<Shop> localShopList;
	private Activity localAct;
	private LayoutInflater li;
		
	
	public ShopSpinnerAdapter(Activity act, ArrayList<Shop> shopList){
		this.localAct = act;
		this.localShopList = shopList;
		li = (LayoutInflater) localAct.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public int getCount() {
		return localShopList.size();
	}

	@Override
	public Object getItem(int position) {
		return localShopList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
//		TextView txtView = new TextView(localAct.getApplicationContext());
//		txtView.setText(getItem(position).toString());
//		return txtView;
		
		return getDropDownView(position, convertView, parent);
		
	}
	
	public static class ViewHolder{
		public TextView spinnerItem;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isEmpty() {
		return localShopList.isEmpty();
	}


	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		
		if(v == null){
			holder = new ViewHolder();
			
			v = li.inflate(R.layout.shop_list_spinner_item, null);
			holder.spinnerItem = (TextView) v.findViewById(R.id.spinnerTxtItem);
			v.setTag(holder);
		}else{
			holder = (ViewHolder) v.getTag();
		}
		
		
		Shop aShop = localShopList.get(position);
		if(aShop != null){
			
			holder.spinnerItem.setText(aShop.getId() + " - " + aShop.getName());
		}
		
		
		
		return v;
	}

	
	
	
}
