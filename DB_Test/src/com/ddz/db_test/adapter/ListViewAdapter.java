package com.ddz.db_test.adapter;

import java.util.ArrayList;

import com.ddz.db_test.R;
import com.ddz.db_test.model.Product;
import com.ddz.db_test.model.Shop;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ListViewAdapter extends BaseAdapter {

	private final int LIST_TYPE;
	
	private ArrayList<Shop> shopList;
	private ArrayList<Product> prodList;
	private Activity activity;
	private LayoutInflater li;
	
	public ListViewAdapter(Activity act, ArrayList<Shop> AShopList){
		this.activity = act;
		this.shopList = new ArrayList<Shop>();
		this.shopList = AShopList;
		this.LIST_TYPE = 0;
		li = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public ListViewAdapter(Activity act, ArrayList<Product> AProdList, int prodType){
		this.activity = act;
		this.prodList = new ArrayList<Product>();
		this.prodList = AProdList;
		this.LIST_TYPE = prodType;
		li = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		switch (LIST_TYPE) {
		case 0:
			return shopList.size();

		default:
			return prodList.size();
		}

	}

	@Override
	public Object getItem(int position) {
		switch (LIST_TYPE) {
		case 0:
			return shopList.get(position);

		default:
			return prodList.get(position);
		}

	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		
		if(v == null){
			holder = new ViewHolder();
			
			v = li.inflate(R.layout.listview_item, null);
			holder.listParentItem = (LinearLayout) v.findViewById(R.id.listParentItem);
			v.setTag(holder);
		}else{
			holder = (ViewHolder) v.getTag();
		}
		
		switch (LIST_TYPE) {
		case 0:
			
			Shop aShop = shopList.get(position);
			
			if(aShop != null){
				
				TextView shopListTxtView = (TextView) v.findViewById(R.id.listViewItem);
				shopListTxtView.setText("ID #" + aShop.getId() + " - " + aShop.getName());
				
				if(position % 2 == 0){
					shopListTxtView.setBackgroundResource(R.color.blue2);
				}else{
					shopListTxtView.setBackgroundResource(R.color.blue3);
				}
				
			}
			
			break;

		default:
			
			Product aProd = prodList.get(position);
			
			if(aProd != null){
				
				TextView prodListTxtView = (TextView) v.findViewById(R.id.listViewItem);
				prodListTxtView.setText("ID #" + aProd.getId() + " - " + aProd.getName());
				
				if(position % 2 == 0){
					prodListTxtView.setBackgroundResource(R.color.blue2);
				}else{
					prodListTxtView.setBackgroundResource(R.color.blue3);
				}
				
			}
			
			break;
		}

		
		return v;
	}
	
	
	public static class ViewHolder{
		public LinearLayout listParentItem;
	}

	
}
