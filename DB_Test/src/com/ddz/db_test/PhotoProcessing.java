package com.ddz.db_test;

import java.io.IOException;

import com.googlecode.tesseract.android.TessBaseAPI;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;


public class PhotoProcessing extends Activity {

	
	// You need to put the data files in the tessdata directory, and specify
	// the parent directory of tessdata in your init()
	private static final String DATA_PATH 	= "/storage/sdcard0/tesseract/";
	private static final String LANG		= "eng";
	private String currentPhotoPath;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_processing);
				
		Bundle extras = getIntent().getExtras();

		if(extras != null){
			currentPhotoPath = extras.getString("PhotoPath");
			Log.d("DDZ", "PhotoProcessAct - Variable currentPhotoPath = "+ currentPhotoPath);
		}
		
		ImageView photo = (ImageView) findViewById(R.id.photoProcessImgView);
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		Bitmap photoBmp = BitmapFactory.decodeFile(currentPhotoPath, options);
//		photo.setImageBitmap(photoBmp);
		
		
		/// OCR CODE ///
		try {
			ExifInterface exif = new ExifInterface(currentPhotoPath);
			int exifOrientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			
			int rotate = 0;
			
			switch (exifOrientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
				
			default:
				break;
			}
			
			if(rotate != 0){
				int w = photoBmp.getWidth();
				int h = photoBmp.getHeight();
				Log.d("DDZ", "photoWidth=" + w + ", photoHeight="+h);
				
				// Setting pre rotate
				Matrix mtx = new Matrix();
				mtx.preRotate(rotate);
				
				// Rotating Bitmap & convert to ARGB_8888, required by Tess
				photoBmp = Bitmap.createBitmap(photoBmp, 0, 0, w, h, mtx, false);
			}
			
			photoBmp = photoBmp.copy(Bitmap.Config.ARGB_8888, true);
			
			TessBaseAPI baseAPI = new TessBaseAPI();
			baseAPI.init(DATA_PATH, LANG);
			baseAPI.setImage(photoBmp);
			String recognizedText = baseAPI.getUTF8Text();
			Log.d("DDZ", "Recognised Text= "+recognizedText);
			baseAPI.end();
			/// END - OCR CODE ///
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.photo_processing, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
