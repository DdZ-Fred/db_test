package com.ddz.db_test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.ddz.db_test.adapter.ListViewAdapter;
import com.ddz.db_test.adapter.ShopSpinnerAdapter;
import com.ddz.db_test.dao.MyHelper;
import com.ddz.db_test.model.Product;
import com.ddz.db_test.model.Shop;
import com.googlecode.tesseract.android.TessBaseAPI;

import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final String DATA_PATH = Environment
			.getExternalStorageDirectory().toString() + "/DB_Test/";
	private static final String LANG = "eng";

	private static final int CAMERA_CAPTURE_ACTIVITY = 1;
	private static final int PHOTO_PROCESSING_ACTIVITY = 2;
	private static final int PHOTO_PROCESSING_ACTIVITY_2 = 3;

	private ArrayList<Shop> shopsList = new ArrayList<Shop>();
	private ArrayList<Product> prodsList = new ArrayList<Product>();
	private ListViewAdapter shopListAdapt;
	private ListViewAdapter prodListAdapt;
	private ShopSpinnerAdapter shopSpinnerAdapter;
	private Spinner shopListSpin;

	private String currentPhotoPath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initDirsAndFiles();
		stylingProcess();

		MyHelper.getInstance(getApplicationContext());
		shopsList = MyHelper.ShopDAO.getAllShops();
		prodsList = MyHelper.ProdDAO.getAllProds();

		Button addShopButton = (Button) findViewById(R.id.addShopButton);
		addShopButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				EditText shopNameInput = (EditText) findViewById(R.id.shopNameInput);
				String inputTxt = shopNameInput.getText().toString();

				if (inputTxt.isEmpty() || inputTxt == null) {

					int duration = Toast.LENGTH_SHORT;
					CharSequence text = "Please type something first! thanks!";
					Toast toast = Toast.makeText(getApplicationContext(), text,
							duration);
					toast.show();

				} else {

					shopsList.add(MyHelper.ShopDAO.addShop(inputTxt));

					shopListAdapt.notifyDataSetChanged();

					if (shopsList.size() == 1) {
						shopListSpin.setSelection(0, true);
					}
					shopSpinnerAdapter.notifyDataSetChanged();
					shopNameInput.setText("");

				}
				hideSoftKeyboard();
			}
		});

		shopListAdapt = new ListViewAdapter(this, shopsList);

		ListView shopListView = (ListView) findViewById(R.id.shopListView);
		shopListView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});
		shopListView.setAdapter(shopListAdapt);

		shopListSpin = (Spinner) findViewById(R.id.prodShopSpinner);
		shopSpinnerAdapter = new ShopSpinnerAdapter(this, shopsList);
		shopListSpin.setAdapter(shopSpinnerAdapter);
		shopListSpin.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long rowid) {
				shopListSpin.setSelection(position, true);
				Log.d("SPINNER SELECTION",
						((Shop) shopListSpin.getSelectedItem()).toString());

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		Button addProdButton = (Button) findViewById(R.id.addProdButton);
		addProdButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				EditText pName = (EditText) findViewById(R.id.prodNameInput);
				String pNameStr = pName.getText().toString();
				Spinner pShopSpin = (Spinner) findViewById(R.id.prodShopSpinner);
				Shop pShop = (Shop) pShopSpin.getSelectedItem();
				EditText pBCode = (EditText) findViewById(R.id.prodBCodeInput);
				String pBCodeStr = pBCode.getText().toString();
				EditText pPrice = (EditText) findViewById(R.id.prodPriceInput);
				String pPriceStr = pPrice.getText().toString();

				if (!pNameStr.isEmpty() && pShop != null
						&& !pBCodeStr.isEmpty() && !pPriceStr.isEmpty()) {

					prodsList.add(MyHelper.ProdDAO.addProd(pNameStr,
							pShop.getId(), Integer.parseInt(pBCodeStr),
							Float.valueOf(pPriceStr)));
					prodListAdapt.notifyDataSetChanged();
					pName.setText("");
					pBCode.setText("");
					pPrice.setText("");

				} else {
					String mess = "";
					if (pNameStr.isEmpty())
						mess += "The name field is empty!";
					if (pShop == null) {
						if (mess.isEmpty()) {
							mess += "You have to choose an existing shop";
						} else {
							mess += "\nYou have to choose an existing shop!";
						}
					}
					if (pBCodeStr.isEmpty()) {
						if (mess.isEmpty()) {
							mess += "The barcode field is empty!";
						} else {
							mess += "\nThe barcode field is empty!";
						}
					}
					if (pPriceStr.isEmpty()) {
						if (mess.isEmpty()) {
							mess += "The price field is empty!";
						} else {
							mess += "\nThe price field is empty!";
						}
					}

					Toast toast = Toast.makeText(getApplicationContext(), mess,
							Toast.LENGTH_LONG);
					toast.show();

				}
				hideSoftKeyboard();
			}
		});

		prodListAdapt = new ListViewAdapter(this, prodsList, 1);
		ListView prodListView = (ListView) findViewById(R.id.prodListView);
		prodListView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});
		prodListView.setAdapter(prodListAdapt);

		Button bCodeReaderLauncher = (Button) findViewById(R.id.prodBCode_LaunchReaderButton);
		bCodeReaderLauncher.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			}
		});

		Button priceReaderLauncher = (Button) findViewById(R.id.prodPrice_LaunchReaderButton);
		priceReaderLauncher.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dispatchTakePictureIntent();

			}
		});
	
	
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (!Debug.isDebuggerConnected()) {
			Debug.waitForDebugger();
			Log.d("DEBUG_ACTIVITY_RESULT", "OK");
		}

		switch (requestCode) {

		case CAMERA_CAPTURE_ACTIVITY:

			switch (resultCode) {
			case RESULT_OK:

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				Bitmap photoBmp = BitmapFactory.decodeFile(currentPhotoPath,
						options);

				// / OCR CODE ///
				try {
					ExifInterface exif = new ExifInterface(currentPhotoPath);
					int exifOrientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION,
							ExifInterface.ORIENTATION_NORMAL);

					int rotate = 0;

					switch (exifOrientation) {
					case ExifInterface.ORIENTATION_ROTATE_90:
						rotate = 90;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						rotate = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						rotate = 270;
						break;

					default:
						break;
					}

					if (rotate != 0) {
						int w = photoBmp.getWidth();
						int h = photoBmp.getHeight();
						Log.d("DDZ", "photoWidth=" + w + ", photoHeight=" + h);

						// Setting pre rotate
						Matrix mtx = new Matrix();
						mtx.preRotate(rotate);

						// Rotating Bitmap & convert to ARGB_8888, required by
						// Tess
						photoBmp = Bitmap.createBitmap(photoBmp, 0, 0, w, h,
								mtx, false);
					}

					photoBmp = photoBmp.copy(Bitmap.Config.ARGB_8888, true);

					TessBaseAPI baseAPI = new TessBaseAPI();
					baseAPI.init(DATA_PATH, LANG);
					baseAPI.setImage(photoBmp);
					String recognizedText = baseAPI.getUTF8Text();
					Log.d("DDZ", "Recognised Text= " + recognizedText);
					baseAPI.end();
					// / END - OCR CODE ///

					EditText priceInput = (EditText) findViewById(R.id.prodPriceInput);
					priceInput.setText(recognizedText);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				break;
			case RESULT_CANCELED:
				break;
			default:
				break;
			}

			break;

		case PHOTO_PROCESSING_ACTIVITY:
			switch (resultCode) {

			case RESULT_OK:
				break;
			case RESULT_CANCELED:
				break;
			default:
				break;
			}

		case PHOTO_PROCESSING_ACTIVITY_2:

			switch (resultCode) {

			case RESULT_OK:
				break;

			case RESULT_CANCELED:
				break;

			default:
				break;

			}

		default:
			break;
		}
	}

	private void stylingProcess() {
		TextView shopTitle = (TextView) findViewById(R.id.shopManagementTitle);
		TextView shopListTitle = (TextView) findViewById(R.id.shopListTitle);
		shopTitle.setPaintFlags(shopTitle.getPaintFlags()
				| Paint.UNDERLINE_TEXT_FLAG);
		shopListTitle.setPaintFlags(shopListTitle.getPaintFlags()
				| Paint.UNDERLINE_TEXT_FLAG);

		TextView prodTitle = (TextView) findViewById(R.id.prodManagementTitle);
		TextView prodListTitle = (TextView) findViewById(R.id.prodsListTitle);
		prodTitle.setPaintFlags(prodTitle.getPaintFlags()
				| Paint.UNDERLINE_TEXT_FLAG);
		prodListTitle.setPaintFlags(prodListTitle.getPaintFlags()
				| Paint.UNDERLINE_TEXT_FLAG);
	}

	private void dispatchTakePictureIntent() {

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		// Ensure there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

			// Temp file where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
				Log.d("DDZ", "Variable currentPhotoPath(tempFile) = "
						+ currentPhotoPath);
				Log.d("DDZ", "PhotoFile path(getPath) = " + photoFile.getPath());
			} catch (IOException e) {
				Log.d("CREATION_FILE_ERROR", e.getMessage());
				e.printStackTrace();
			}

			// If the file was successfully created then...
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent,
						CAMERA_CAPTURE_ACTIVITY);
			}

		}

	}

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {

		String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss")
				.format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

		File image = File.createTempFile(imageFileName, ".jpg", storageDir);

		// currentPhotoPath = "file:" + image.getAbsolutePath();
		currentPhotoPath = image.getAbsolutePath();
		return image;
	}

	// private String randomStrGeneration() {
	//
	// String chars =
	// "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_-$%";
	// Random rdm = new Random();
	// int length = 15;
	// char[] text = new char[length];
	// for (int i = 0; i < length; i++) {
	// text[i] = chars.charAt(rdm.nextInt(chars.length()));
	// }
	//
	// return new String(text);
	// }
	
	public void initDirsAndFiles(){
		String[] paths = new String[] { DATA_PATH, DATA_PATH + "tessdata/"};
		
		// Creation of the dirs to store tesseract files
		for(String path: paths){
			File dir = new File(path);
			if(!dir.exists()){
				// "mkdirs" will create the complete path, if parents folders are necessary, it will be created.
				// unlike "mkdir"
				if(!dir.mkdirs()){
					Log.v("DDZ", "ERROR: Creation of directory " + path + " on scard failed");
				}else{
					Log.v("DDZ", "Creation of directory " + path + " on sdcard successful!");
				}
			}else{
				Log.v("DDZ", "Directory " + path + " already exists");
			}
		}
		
		File engDictionnary = new File(DATA_PATH + "tessdata/" + LANG + ".traineddata");
		if(!engDictionnary.exists()){
			
			// AssetManager initialization
			AssetManager am = getAssets();
			InputStream in = null;
			OutputStream out = null;
			
			try {
				
				// Open my asset file / "in" init
				in = am.open("tessdata/" + LANG + ".traineddata");
				
				// It will write/construct the file on the FileSystem
				out = new FileOutputStream(engDictionnary);
				copyFile(in, out);
				
				in.close();
				in = null;
				out.flush();
				out.close();
				out = null;
				
			} catch (Exception e) {
				Log.e("DDZ", "Error during the copy of the " + LANG + " dictionnary");
			}
			
		}else{
			Log.v("DDZ", "Tesseract english dictionnary already on the phone");
		}
		
	}
	
	private void copyFile(InputStream in, OutputStream out) throws IOException{
		try {
			// Represent 1Ko/1KB
			byte[] buffer = new byte[1024];
			int read;
			
			// So, reading/writing KB by KB
			while((read = in.read(buffer)) != -1){
				out.write(buffer, 0, read);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void hideSoftKeyboard(){
		InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
	}
	
}
