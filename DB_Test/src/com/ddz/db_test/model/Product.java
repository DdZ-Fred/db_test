package com.ddz.db_test.model;

import java.io.Serializable;

public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private int barcode;
	private float price;
	private long shopId;
	
	
	public Product(){}

	public Product(String prodName){
		name = prodName;
	}
	
	public Product(long prodId, String prodName, long prodShopId, int prodBCode, float prodPrice){
		this.id = prodId;
		this.name = prodName;
		this.shopId = prodShopId;
		this.barcode = prodBCode;
		this.price = prodPrice;

	}
	
	public Product(String prodName, long prodShopId, int prodBCode, float prodPrice){
		this.name = prodName;
		this.shopId = prodShopId;
		this.barcode = prodBCode;
		this.price = prodPrice;

	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getBarcode() {
		return barcode;
	}


	public void setBarcode(int barcode) {
		this.barcode = barcode;
	}


	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public long getShopId() {
		return shopId;
	}


	public void setShopId(long shopId) {
		this.shopId = shopId;
	}
	
	

}
