package com.ddz.db_test.model;

import java.io.Serializable;

public class Shop implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	
	public Shop(){}
	
	public Shop(String shopName){
		this.name = shopName;
	}
	
	public Shop(long shopId, String shopName){
		this.id = shopId;
		this.name = shopName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return this.id+ " - "+this.name;
	}
	
}
